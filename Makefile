APPDATA = \
  sm.puri.librem5.goodies.appdata.xml \
  $(NULL)

all: shell-check appstream-check

shell-check:
	shellcheck l5-contacts-importer l5-screenshot l5-scale-the-screen

appstream-check:
	appstream-util validate-relax $(APPDATA)

.PHONY: all shell-check appstream-check
