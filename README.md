# Librem 5 Goodies

This project serves to create a deb package with several bash scripts created for the Librem 5 phone.

Bring your script here and it might be packaged :tada:

Please Avoid scripts that require cloning and building packages from source.

#### Scripts added to this package:

---

|Script | Description | Dependencies | Notes |
|:--:|:--:|:--:|:--:|
|Contacts Importer| Allows to import contacts from a vcard file to GNOME Contacts. Can work with a UI and from command line interface|`yad`, `syncevolution`, `libnotify-bin`, `Evolution Data Server`| This script will be removed from this paclage when the version of GNOME-Contacts available in PureOS supports importing contacts from a vcard file. |
|Screenshot| Allows to take screenshots in the Librem 5 phone|`yad`, `grim`, `libnotify-bin`, `xdg-user-dirs`| This script will be removed from this package when there is a native app that can take screenshots in `Phosh`. |
|Scale the screen| A simple application to scale the screen| `wlr-randr`, `yad` | This script will be removed from this package when `phosh` has better support for screen scaling.|



#### Adding a script to this package:

- Make a new branch based on the master branch and work from it.
- Make a merge request
- Avoid scripts that download and build source code. If you need a dependency not available in PureOS, package it :D
- if you are not confortable in commiting to a Deb package then open an issue asking for the script to be packaged and we will work it out
